# Chaîne de traitement Presto

## Dépendances
Conçu et testé sur Ubuntu 16.04. Devrait fonctionner sur toute distribution Linux à base Debian disposant de _sudo_.

## Installation
bash install.sh

## Utilisation
bash presto.sh

## Crédits
Développement: Achille Falaise

Financement:
* projet Presto, ANR, laboratoire ICAR
* labex ASLAN, CNRS, laboratoire ICAR

## Licence
Licence GPL v3
