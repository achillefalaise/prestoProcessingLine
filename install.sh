#!/usr/bin/env bash

# Script d'installation pour Ubuntu (fonctionne aussi sur Denécessite sudo).
# Usage: bash install.sh
# Lancer le script en tant qu'utilisateur normal, inutile d'être root. Le mot de passe sudo sera demandé pendant l'exécution.

TREETAGGER_VERSION=3.2.1
TALISMANE_VERSION=5.1.1

# Aller dans le dossier où se trouve le script d'installation
BASE_DIR=$(dirname "$0")
cd "$BASE_DIR"

# Créer le .gitignore
echo -e "components/presto*\ncorpora/\nlib/\nprofiles/" > .gitignore

# Ajouter les composants Presto (dépôts à part)
mkdir -p components
(
  cd components
  for compo in prestoLexicalRules prestoNormaliser prestoTokeniser; do
    if [ -d $compo ]; then
      git pull https://gitlab.com/achillefalaise/$compo.git
    else
      git clone https://gitlab.com/achillefalaise/$compo.git
    fi
  done
)

# Installer les dépendances (testé sur Ubuntu 16.04)
sudo apt install python3-csvkit rsync
sudo cpan -i File::Basename File::Slurp Getopt::Long HTML::Escape HTML::TokeParser::Simple JSON::XS Roman Switch Text:CSV_XS Try::Tiny XML::Entities

# Dossiers pour les corpus et les profils
mkdir -p corpora
mkdir -p profiles

######################
# Ajouter les outils #
######################

# TreeTagger
( 
  mkdir -p lib/treetagger/$TREETAGGER_VERSION
  cd /tmp
  if [ ! -f tree-tagger-linux-$TREETAGGER_VERSION.tar.gz ]; then
    wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/Tagger-Licence
    cat Tagger-Licence
    # Todo message pour accepter la licence
    wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger-linux-$TREETAGGER_VERSION.tar.gz
  fi
  tar -xvzf tree-tagger-linux-$TREETAGGER_VERSION.tar.gz
)
rsync -a /tmp/bin/train-tree-tagger ./lib/treetagger/$TREETAGGER_VERSION
rsync -a /tmp/bin/tree-tagger ./lib/treetagger/$TREETAGGER_VERSION

# Talismane
(
  mkdir -p lib/talismane/$TALISMANE_VERSION
  cd /tmp
  if [ ! -f talismane-distribution-$TALISMANE_VERSION-bin.zip ]; then
    wget https://github.com/joliciel-informatique/talismane/releases/download/v5.1.1/talismane-distribution-$TALISMANE_VERSION-bin.zip
  fi
  unzip talismane-distribution-$TALISMANE_VERSION-bin.zip -d talismane
)
rsync -a /tmp/talismane/* ./lib/talismane/$TALISMANE_VERSION
rm -r /tmp/talismane
