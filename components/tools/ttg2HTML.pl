#!/usr/bin/perl -w

use strict; 
use warnings;

use HTML::Escape qw/escape_html/;

# Tout passer en UTF-8
use v5.14;
use utf8;
binmode STDOUT, ':utf8';
binmode STDIN, ":utf8";  
use open qw(:std :utf8);
use Encode qw(decode_utf8);

print "
<html>
<head>
<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"/>
<style>
table.header {background-color: silver; border: 1px solid gray; padding: 5px; margin: 10px; font-size: smaller} 
  p {margin: 10px;} 
  .unklemma {border: 1px solid red} 
  foreign {font-style: italic} 
  .word {display: inline-block; margin: 1px; padding: 1px}
  .word[shortmorph=N] {background-color: #c8dcff}
  .word[shortmorph=V] {background-color: #ceffc8} 
  .word[shortmorph=A] {background-color: #ffd6c8} 
  .word[shortmorph=P] {background-color: #c8fdff} 
  .word[shortmorph=D] {background-color: #ddd} 
  .word[shortmorph=G] {background-color: #ffc8c8}
  .word[shortmorph=R] {background-color: #ffe4c8}
  .word[shortmorph=S] {background-color: #f1ffc8}
  .word[shortmorph=C] {background-color: #c8fff0}
  .word[shortmorph=M] {background-color: #e0c8ff}
  .word[shortmorph=X] {background-color: #fcc8ff}
</style></head><body>";

while (my $line = <STDIN>) {
  chomp($line);
   
  if($line!~m/^\s*#/ && $line!~m/^\s*</) {  # Tout traiter sauf lignes qui commencent par # ou <

    # Détection  
    my $form = '';
    my $lemma = '';
    my $morph = '';
    my $sem = '';
    if($line=~m/^([^\t]+?)\t([^\t]+?)\t([^\t]+?)\t([^\t]+?)$/) {
      ($form, $morph, $lemma, $sem) = split(/\s*\t\s*/, $line);
    }
    elsif($line=~m/^([^\t]+?)\t([^\t]+?)\t([^\t]+?)$/) {
      ($form, $morph, $lemma) = split(/\s*\t\s*/, $line);
    }
    elsif($line=~m/^([^\t]+?)\t([^\t]+?)$/) {
      ($form, $morph) = split(/\s*\t\s*/, $line);
    }
    else {
      $form = $line;
    }

    # Mise en forme    
    my $formHtml = escape_html($form);
    my $lemmaHtml = escape_html($lemma);
    my $morphHtml = escape_html($morph);
    my $semHtml = escape_html($sem);
    my $shortMorph = $morph=~s/[a-z]//gr;
    my $classes = 'word ';
    if($lemma eq '_unknown_') {
      $classes .= 'unklemma';
    }
    print "<span title=\"$morphHtml\t$lemmaHtml\t$semHtml\" morph=\"$morphHtml\" shortmorph=\"$shortMorph\" lemma=\"$lemmaHtml\" sem=\"$semHtml\" class=\"$classes\">$formHtml</span>\n";
    
  }
  
  elsif($line=~m/\s*<presto:text/) {
    print $line;
    print '<table class="header">';
    foreach my $meta ($line=~m/([a-z]+="?[^"]+"?)/gi) {
      $meta=~m/^(.+?)="?([^"]+)"?/;
      print '<tr><td class="key">'.$1.'</td><td class="value">'.$2.'</td></tr>';
    }
    print '</table>';
  }
  
  elsif($line=~m/\s*<\/text>/) {
    print $line;
  }
  
  else {
    print "$line\n";
  }
  
}

print "</body></html>";
