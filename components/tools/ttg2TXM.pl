#!/usr/bin/perl -w

use strict; 
use warnings;

use Getopt::Long;

# Tout passer en UTF-8
use v5.14;
use utf8;
binmode STDOUT, ':utf8';
binmode STDIN, ":utf8";  
use open qw(:std :utf8);
use Encode qw(decode_utf8);
use File::Slurp;

# Lecture des arguments
my $config_csv = 0;
my $config_id = '';
GetOptions(
  'csv|c=s'=>\$config_csv,
  'id|i=s'=>\$config_id
);

if(!$config_csv) {
  print STDERR "Il faut indiquer un fichier CSV de sortie (option -c).\n";
  die();
}

my $fh;
if(! -e $config_csv) {  # Le CSV n'existe pas encore
  open($fh, '>', $config_csv) or die($!);
  print $fh join(',', ('id', 'type', 'author', 'title', 'century', 'dateFirstEdition', 'dateCopyEdition', 'genre', 'theme', 'licence', 'source', 'licenceType', 'decade')) . "\n";
}
else {  # Le CSV existe déjà
  open($fh, '>>', $config_csv) or die($!);
}

while (my $line = <STDIN>) {
  chomp($line);
  
  if($line!~m/^\s*#/ && $line!~m/^\s*</) {  # Tout traiter sauf lignes qui commencent par # ou <
    $line=~s/<.*?>//g;  # Virer toutes les balises
  
    $line=~s/&/&amp;/g;
    $line=~s/"/&quot;/g;

    if($line=~m/^([^\t]+?)\t([^\t]+?)\t([^\t]+?)\t([^\t]+?)$/) {    # TODO Récupérer la liste des tags directement dans le header du texte
      my ($form, $morph, $lemma, $sem) = split(/\s*\t\s*/, $line);
      my $shortMorph = $morph=~s/^(.(\+.)?).*/$1/r;
      print "<w pos=\"$morph\" spos=\"$shortMorph\" lemma=\"$lemma\" sem=\"$sem\">$form</w>\n";
    }  
    elsif($line=~m/^([^\t]+?)\t([^\t]+?)\t([^\t]+?)$/) {
      my ($form, $morph, $lemma) = split(/\s*\t\s*/, $line);
print STDERR "$line\n";
      my $shortMorph = $morph=~s/^(.(\+.)?).*/$1/r;
      print "<w pos=\"$morph\" spos=\"$shortMorph\" lemma=\"$lemma\">$form</w>\n";
    }
    elsif($line=~m/^([^\t]+?)\t([^\t]+?)$/) {
      my ($form, $morph) = split(/\s*\t\s*/, $line);
      my $shortMorph = $morph=~s/^(.(\+.)?).*/$1/r;
      print "<w pos=\"$morph\" spos=\"$shortMorph\" lemma=\"\">$form</w>\n";
    }
    elsif($line=~m/^([^\t]+?)$/) {
      print "\n";
    }
  }
  else {  # lignes qui commencent par # ou <
    if($line=~m/<presto:text /) {
      my $id = $config_id;
      $line=~s/<presto:text /<text /g;
      print "$line\n";
        
      print $fh "$id";
      foreach my $key ('type', 'author', 'title', 'century', 'dateFirstEdition', 'dateCopyEdition', 'genre', 'theme', 'licence', 'source', 'licenceType', 'decade') {  # TODO Récupérer la liste des champs directement dans le header du texte
        print $fh ',';
        if($line=~m/$key="(.*?)"/) {
          my $val = $1;
          print $fh "\"$val\"";
        }
      }
      print $fh "\n";
      
    }
    elsif($line=~m/<\/presto:text>/) {
      print "</text>";
    }
      
  }
    
}

close($fh);
